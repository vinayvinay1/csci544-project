__author__ = 'vinaykumar'

import sys

f1 = open(sys.argv[1], 'r')
f2 = open(sys.argv[2], 'r')
fout = open(sys.argv[3], 'w')

file1 = f1.read().splitlines()
file2 = f2.read().splitlines()

errors = 0
counter = 0

for i in range(len(file1)):
    counter += 1
    if file1[i][0] != file2[i][0]:
        errors += 1
    fout.write(file1[i]+" "+file2[i][2:])
    fout.write("\n")

f1.close()
f2.close()
fout.close()

print(counter, " ", errors)