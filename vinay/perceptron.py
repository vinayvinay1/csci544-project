__author__ = 'vinaykumar'

import sys
import random
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score

w = {}
wavg = {}
classes = {}
headers = set()
features = set()

fin = open(sys.argv[1], 'r', errors='ignore')
ftest = open(sys.argv[2], 'r').read().splitlines()

lines = fin.read().splitlines()
fin.close()

# Build w vector for all classes and words in the training data
for line in lines:
    words = line.split()
    headers.add(words[0])
    features |= set(words[1:])

for h in headers:
    w.update({h: {}})
    wavg.update({h: {}})
    classes.update({h: 0})

for h in headers:
    for f in features:
        w[h].update({f: 0})
        wavg[h].update({f: 0})

counter = 0
for x in range(10):
    random.shuffle(lines)
    for line in lines:
        temp = dict(classes)
        words = line.split()
        for word in words[1:]:
            for h in temp:
                temp[h] += w[h][word]

        tests = dict(temp)

        true_tag = words[0]
        pred_tag = max(temp, key=temp.get)

        if pred_tag != true_tag:
            for word in words[1:]:
                w[pred_tag][word] -= 1
                wavg[pred_tag][word] -= counter
                w[true_tag][word] += 1
                wavg[true_tag][word] += counter
        counter += 1

    for h in headers:
        for f in features:
            wavg[h][f] = w[h][f] - (wavg[h][f]/counter)

classes = {}
y_pred=[]
y_test = [d[0] for d in ftest]

for h in headers:
    classes.update({h: 0})

for line in ftest:
    for h in headers:
        for word in line.split()[1:]:
            if word in wavg[h]:
                classes[h] += wavg[h][word]
    lbl = max(classes, key=classes.get)
    y_pred.append(lbl)

print(accuracy_score(y_pred, y_test))
print(f1_score(y_test, y_pred, pos_label=classes, average=None))
