__author__ = 'vinaykumar'

import numpy as np
import numpy
from sklearn import svm
from sklearn.metrics import f1_score
from sklearn.metrics import accuracy_score
from sklearn.feature_extraction.text import TfidfVectorizer
import sys

def tfidf_data(docs):
    y = [d[0] for d in docs]
    corpus = [d[1:] for d in docs]
    return y, corpus

training_docs = open(sys.argv[1], 'r').read().splitlines()
predict_docs = open(sys.argv[2], 'r').read().splitlines()

y_train, corpus_train = tfidf_data(training_docs)
y_test, corpus_test = tfidf_data(predict_docs)

# Create and fit a TFIDF vectorizer
vectorizer = TfidfVectorizer(min_df=1)
X_train = vectorizer.fit_transform(corpus_train)
X_test = vectorizer.transform(corpus_test)

# Train a SVM based on Training data obtained in TF-IDF format
clf = svm.SVC()
clf.fit(X_train, y_train)

y_pred = clf.predict(X_test)

print(clf.score(X_test, y_test))
print(accuracy_score(y_pred, y_test))
f1 = f1_score(y_test, y_pred,pos_label=['d', 't'], average=None)
print(f1)