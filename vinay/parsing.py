__author__ = 'vinaykumar'

import os
import sys
import nltk
from nltk import ParentedTree
from nltk import parse
from nltk.tree import Tree
from nltk.parse import stanford

os.environ['STANFORD_PARSER'] = 'jars/'
os.environ['STANFORD_MODELS'] = 'jars/'
parser = stanford.StanfordParser(model_path="edu/stanford/nlp/models/lexparser/englishPCFG.ser.gz")

training_docs = open(sys.argv[1], 'r').read().splitlines()
feature_docs = open(sys.argv[2], 'w')

for doc in training_docs:
    label = doc[0]
    feature_docs.write(label)
    content = doc[2:]
    parsed_doc = parser.raw_parse_sents(nltk.sent_tokenize(content))

    for line in parsed_doc:
        for t in line:
            ptree = ParentedTree.convert(t)

            for st in ptree.subtrees(lambda p: p.height() == 2):
                feature_word = " "+st.parent().label()+":"+st.label()+":"+st[0]
                feature_docs.write(feature_word)
    feature_docs.write("\n")

feature_docs.close()



