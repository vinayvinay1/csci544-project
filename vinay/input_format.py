__author__ = 'vinaykumar'

import sys
import glob

files = glob.glob(sys.argv[1]+"*.txt")
output = open(sys.argv[2], 'w')

for f in files:
    label = f.split("/")[-1].split("_")[0]
    doc = open(f, 'r').read()
    output.write(label+" "+doc)

output.close()