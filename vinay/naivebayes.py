__author__ = 'vinaykumar'

import sys
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score
import math

kvalue = 0
stotal = 0
kd = {}
classes = []
ncd = {}
pcd = {}
nid = {}

svm = {}

fin = open(sys.argv[1], 'r').read().splitlines()
ftest = open(sys.argv[2], 'r').read().splitlines()

for line in fin:
    templine = line.split()
    stotal += 1

    uniquewords = set(templine[1:])

    for w in uniquewords:
        if w not in svm:
            svm.update({w: 1})

        else:
            svm[w] += 1

    if templine[0] not in pcd:
        pcd.update({templine[0]: 1})
        ncd.update({templine[0]: 0})
        nid.update({templine[0]: {}})
        classes.append(templine[0])

    else:
        pcd[templine[0]] += 1

    gen = (words for words in templine[1:])
    for words in gen:

        # update individual class word count Nc
        ncd[templine[0]] += 1

        # update ni count for all words wrt each class
        if words not in nid[templine[0]]:
            nid[templine[0]].update({words: 2})

        else:
            nid[templine[0]][words] += 1

        # find value of K (total vocab count)
        if words not in kd:
            kvalue += 1
            kd.update({words: kvalue})

# Calculate P(C) dictionary for all classes
for l in iter(pcd):
    pcd[l] /= stotal

pcmd = {}
y_pred = []
y_test = [d[0] for d in ftest]

for x in classes:
    pcmd.update({x: 0})

for line in ftest:
    templine = line.split()

    for cls in classes:
        b = 0

        for words in templine[1:]:
            if words in nid[cls]:
                b += math.log(nid[cls][words]) - math.log(ncd[cls] + kvalue)

            else:
                b -= math.log(ncd[cls] + kvalue)

        #calculate P(C) of each sample
        pcmd[cls] = math.log(pcd[cls]) + b

    lbl = max(pcmd, key=pcmd.get)
    y_pred.append(lbl)

print(accuracy_score(y_pred, y_test))
print(f1_score(y_test, y_pred, pos_label=classes, average=None))



