INDIVIDUAL CONTRIBUTIONS


Report preparation:

1. “Introduction” and “Related Work” sections were written by Asitang Mishra.
2. “Data” and “Technical Approach” sections were written by Supreeth Sanur.
3. “Evaluation and Analysis” and “Conclusion” sections were written by Vinay Madisetty.



Project contributions:

1. Vinay Madisetty

--Reorganizing dataset files to program code accessible formats that contain documents and their corresponding Classification labels.

--Preparation of PCFG parsed tree features using NLTK.

--Preparing scripts for SVM, Naive-Bayes and PCFG Sentence Parser.

--Running the Naive-Bayes classifier on each dataset and for each feature and calculating the accuracies.

2. Asitang Mishra

--Collection, organization and merging of 3 types of Essay datasets : Best Friend, Abortion and Death Penalty.

--Preparation of POS tag, n-gram and Psycho-linguistic features.

--Preparing scripts for Averaged-perceptron and Psycho-linguistic feature extraction functions.

--Preparation of Dictionary word lists for each psycho-linguistic feature types.

--Running the Averaged-perceptron classifier on each dataset and for each feature and calculating the accuracies.

3. Supreeth Sanur

--Collection of Hotel Review dataset which comprises positive and negative reviews.

--Preparation of POS tag, n-gram and psycho-linguistic features.

--Preparation of PCFG parsed tree features using Stanford parser.

--Scripts for extracting the analytical features of the psycholinguistic feature set. 

--Running the SVM classifier on each dataset and for each feature and calculating the accuracies.