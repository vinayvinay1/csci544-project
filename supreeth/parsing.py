from stat_parser import Parser
import nltk
from nltk import ParentedTree
import linecache
import codecs
parser = Parser()
'''t= parser.parse("How can the net amount of entropy of the universe be massively decreased?")
print t
print t.leaves()
print t.label()
c=0
for subtree in t.subtrees():
    if any(t.leaves()) in subtree:
        c+=1

print c'''
#f=['How can the net amount of entropy of the universe be massively decreased?']
f=codecs.open('both.train.shuffle.tokenize','r',encoding='utf-8')
#f1=open('parser.all.subtrees.train','w')
i=1
'''for line in f:
    for sentence in nltk.sent_tokenize(line[2:]):
        tree=parser.parse(sentence)
        for subtree in tree.subtrees():
            splitted = subtree.pprint().split()
            flat_tree = ''.join(splitted)
            f1.write(flat_tree+' ')
    print i
    i+=1    
    f1.write('\n')'''

#f.seek(0,0)       

f2=open('parser.leaves.grandparents.train','a')

for line in f:
    for sentence in nltk.sent_tokenize(line[2:]):
        #print(sentence+"\n\n")
        tree=parser.parse(sentence)
        ptree=ParentedTree.convert(tree)
        for subtree in ptree.subtrees(filter = lambda st: st.label() in ['NN','CC','CD','DT','EX','FW','IN','JJ','JJR','JJS','LS','MD','NNS','NNP','NNPS','PDT','POS','PRP','PRP$','RB','RBR','RBS','RP','SYM','TO','UH','VB','VBD','VBG','VBN','VBP','VBZ','WDT','WP','WP$','WRB']):
            f2.write(str(subtree.parent().label())+str(''.join(subtree.pprint().split()))+' ')
    print i
    i+=1
    f2.write('\n')
        

     

f.close()
#f1.close()
f2.close()


