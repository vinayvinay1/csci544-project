import json
from pprint import pprint
import sys
from collections import defaultdict


def classify(modelfile):
    f=open(modelfile,'r')
    model1 = json.load(f)
#temp=[defaultdict(int,model1[k]) for k in model1.keys()]

    model=defaultdict(lambda: defaultdict(int),model1)
#model

#pprint(model)
    f.close()
    doc=input("Enter the document")
    dic_predict=defaultdict(int)
    dic_words=defaultdict(int)
    words=doc.split()
    for w in words:
        dic_words[w]=dic_words[w]+1
    for K in model.keys():
        V=0
        for k,v in dic_words.items():
            V=V+ v*int(model[K].get(k) or 0)
            dic_predict[K]=V
    predicted=max(dic_predict.values())
    for k,v in dic_predict.items():
        if predicted ==v:
            predicted_class=k
            break
    print(predicted_class+'\n')
    sys.stdout.flush()

def main():
    modelfile=sys.argv[1]
    classify(modelfile)

if __name__ == '__main__':
    main()