import nltk
import string


def main():
    #f=open('hotel_train.txt','r')
    print number_count('Isn\'t it great. Affinia Chicago, is one of the: worst6 hotels I have ever stayed at. Not in my life have I been 813 treated so poorly as a guest. The front desk was very unaccommodating when I asked for a smoke free room when they had made an error in my reservation.')

    #for line in f:
        #print word_count(line[2:])
        #print sentence_count(line[2:])
        #print words_per_sentence(line[2:])
        #print big_word_count(line[2:])
        #print punc_count(line[2:])

    #f.close()

def word_count(doc):
    tokens=nltk.word_tokenize(doc)
    return len(tokens)

def sentence_count(doc):
    sentences=nltk.sent_tokenize(doc)
    return len(sentences)

def words_per_sentence(doc):
    token_length=len(nltk.word_tokenize(doc))
    sentences_length=len(nltk.sent_tokenize(doc))
    return int(token_length/sentences_length)

def big_word_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    for word in tokens:
        if len(word)>6:
            count+=1
    return count

def punc_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    punctuations=string.punctuation.replace('.','')
    for word in tokens:
        if word in punctuations:
            print word
            count+=1
    return count

def number_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    for word in tokens:
        flag=0
        for ch in word:
            if ch in '0123456789':
                flag=1
                break
        if flag==1:
            count+=1
    return count




if __name__ == '__main__':
    main()