import sys
import math
import json
from collections import defaultdict
import random
import argparse


def learn(trainfile,devfile,modelfile):
    count_iterations=0
    count_training=0
    x=20
    if devfile=='aaa':
        f1=open(modelfile,'w',errors="ignore")
        dic_z=defaultdict(int)
        dic_model=defaultdict(lambda: defaultdict(int))
        dic_model_avg=defaultdict(lambda: defaultdict(int))
        dic_model_max=defaultdict(lambda: defaultdict(int))
        dic_model_a=defaultdict(lambda: defaultdict(int))    
        max_acc=0
        new_acc=0
        while(count_iterations<=x):
            print('Iterartion ',count_iterations)
            count_iterations=count_iterations+1
            f=open(trainfile,'r',errors="ignore")
            lines=f.read().splitlines()
            if count_training==0:
                c=len(lines)
            random.shuffle(lines)
            correc=0
            for line in lines:
                count_training+=1
                #print(count_training)
                dic_feature=defaultdict(int)
                clas=line.split()[0]
                dic_z[clas]=0
                words=line.split()[1:]
                for w in words:
                    dic_feature[w]=dic_feature[w]+1
                for K,V in dic_z.items():
                    V=0
                    for k,v in dic_feature.items():
                        V=V+ v*dic_model[K][k]
                    dic_z[K]=V
                predicted=max(dic_z.values())
                if predicted==0:
                    predicted_class=sorted(dic_z.keys())[0]
                else:
                    for k,v in dic_z.items():
                        if predicted ==v:
                            predicted_class=k
                            break
                if predicted_class!=clas:
                    for w in dic_feature.keys():
                        dic_model[predicted_class][w]=dic_model[predicted_class][w]-dic_feature[w]
                        dic_model[clas][w]=dic_model[clas][w]+dic_feature[w]
                        dic_model_a[predicted_class][w]=dic_model_a[predicted_class][w]-((c*x-count_training)*dic_feature[w])
                        dic_model_a[clas][w]=dic_model_a[clas][w]+((c*x-count_training)*dic_feature[w])
                else:
                   correc+=1
                        
            f.close()
            max_acc=new_acc
            new_acc=correc/c
            if (new_acc>max_acc):
                max_acc=new_acc
                dic_model_max=dic_model_a.copy()

            print('max accuracy ',max_acc)
            print('new accuracy ',new_acc)

        json.dump(dic_model_max, open(modelfile, 'w'))
        f1.close()

    else:

        f1=open(modelfile,'w',errors="ignore")
        dic_z=defaultdict(int)
        dic_model=defaultdict(lambda: defaultdict(int))
        dic_model_avg=defaultdict(lambda: defaultdict(int))
        dic_model_max=defaultdict(lambda: defaultdict(int))
        dic_model_a=defaultdict(lambda: defaultdict(int))    
        new_accuracy=0
        max_accuracy=0
        while(count_iterations<=x):
            print('Iterartion ',count_iterations)
            count_iterations=count_iterations+1
            f=open(trainfile,'r',errors="ignore")
            lines=f.read().splitlines()
            if count_training==0:
                c=len(lines)
            random.shuffle(lines)
            for line in lines:
                count_training+=1
                #print(count_training)
                dic_feature=defaultdict(int)
                clas=line.split()[0]
                dic_z[clas]=0
                words=line.split()[1:]
                for w in words:
                    dic_feature[w]=dic_feature[w]+1
                for K,V in dic_z.items():
                    V=0
                    for k,v in dic_feature.items():
                        V=V+ v*dic_model[K][k]
                    dic_z[K]=V
                predicted=max(dic_z.values())
                if predicted==0:
                    predicted_class=sorted(dic_z.keys())[0]
                else:
                    for k,v in dic_z.items():
                        if predicted ==v:
                            predicted_class=k
                            break
                if predicted_class!=clas:
                    for w in dic_feature.keys():
                        dic_model[predicted_class][w]=dic_model[predicted_class][w]-dic_feature[w]
                        dic_model[clas][w]=dic_model[clas][w]+dic_feature[w]
                        dic_model_a[predicted_class][w]=dic_model_a[predicted_class][w]-((c*x-count_training)*dic_feature[w])
                        dic_model_a[clas][w]=dic_model_a[clas][w]+((c*x-count_training)*dic_feature[w])
            
           
            f2=open(devfile,'r',errors="ignore")
            print('classify dev')
            correct=0
            dev_file_count=0
            for line in f2:
                dic_predict=defaultdict(int)
                dic_words=defaultdict(int)
                words=line.split()
                for w in words:
                    dic_words[w]=dic_words[w]+1
                for K in dic_model_a.keys():
                    V=0
                    for k,v in dic_words.items():
                        V=V+ v*int(dic_model_a[K].get(k) or 0)
                        dic_predict[K]=V
                predicted=max(dic_predict.values())
                for k,v in dic_predict.items():
                    if predicted ==v:
                        predicted_class=k
                        break
                expected_output=line.split()[0]
                if predicted_class==expected_output:
                    correct=correct+1
                dev_file_count=dev_file_count+1
            
            new_accuracy= correct/dev_file_count
            f2.close()

            if (new_accuracy>max_accuracy):
                max_accuracy=new_accuracy
                dic_model_max=dic_model_a.copy()

            print('max accuracy ',max_accuracy)
            print('new accuracy ',new_accuracy)   
             
        json.dump(dic_model_max, open(modelfile, 'w'))
        f1.close()

def main():
    parser = argparse.ArgumentParser(add_help=False)
    parser.add_argument('-h',dest="inputfiledev",action='store')
    parser.add_argument('inputfiletrain')
    parser.add_argument('modelfile')
    result = parser.parse_args()

    if result.inputfiledev!= None:
        learn(result.inputfiletrain,result.inputfiledev,result.modelfile)
    else:
        devfile='aaa'
        learn(result.inputfiletrain,devfile,result.modelfile)
    
if __name__ == '__main__':
    main()