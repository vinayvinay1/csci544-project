import json
from pprint import pprint
import sys
from collections import defaultdict
import codecs


def classify(modelfile):
    f=open(modelfile,'r')
    model1 = json.load(f)
    model=defaultdict(lambda: defaultdict(int),model1)
    f.close()

    
    sys.stdin = codecs.getreader('utf8')(sys.stdin.detach(), errors='ignore')

    for line in sys.stdin:
        dic_predict=defaultdict(int)
        dic_words=defaultdict(int)
        words=line.split()
        for w in words:
            dic_words[w]=dic_words[w]+1
        for K in model.keys():
            V=0
            for k,v in dic_words.items():
                V=V+ v*int(model[K].get(k) or 0)
                dic_predict[K]=V
        predicted=max(dic_predict.values())
        for k,v in dic_predict.items():
            if predicted ==v:
                predicted_class=k
                break
        print(predicted_class)
        sys.stdout.flush()

def main():
    modelfile=sys.argv[1]
    classify(modelfile)

if __name__ == '__main__':
    main()