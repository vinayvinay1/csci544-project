
import random
import sys

inputfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
outputfile=open(sys.argv[1]+'.shuffle','w')	

linelist=[]
for line in inputfile:
	linelist.append(line)

random.shuffle(linelist)

for line in linelist:
	outputfile.write(line)