import sys
import util

inputfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
outputfile=open(sys.argv[1]+'.9','w')
outputtemp=''
happy=0
sad=0
iword=0
punc=0
insight=0
wordcount=0
sentcount=0
wordbysent=0
bigword=0
conjunc=0
propnoun=0
twothreepron=0
number=0
dic={}

for line in inputfile:
	classname=line.split(' ')[0]
	line=line.replace('\n','')
	temp=''
	wordlist=open("wordlists/happy",'r', encoding='cp1252', errors='ignore')
	happy=util.count(line,wordlist)
	wordlist.close()
	
	wordlist=open("wordlists/sad",'r', encoding='cp1252', errors='ignore')
	sad=util.count(line,wordlist)
	wordlist.close()
	
	wordlist=open("wordlists/iword",'r', encoding='cp1252', errors='ignore')
	iword=util.count(line,wordlist)
	wordlist.close()
	
	wordlist=open("wordlists/insight",'r', encoding='cp1252', errors='ignore')
	insight=util.count(line,wordlist)
	wordlist.close()
	
	wordlist=open("wordlists/twothreepron",'r', encoding='cp1252', errors='ignore')
	twothreepron=util.count(line,wordlist)
	wordlist.close()
	
	wordcount=util.word_count(line)
	
	punc=util.punc_count(line)
	
	sentcount=util.sentence_count(line)
	
	wordbysent=util.words_per_sentence(line)
	
	bigword=util.big_word_count(line)
	
	number=util.number_count(line)
	
	conjunc=util.countPOS(line,'CC')
	
	propnoun=util.countPOS(line,'NP')+util.countPOS(line,'NNP')

	dic.update({"happy":happy})
	dic.update({"sad":sad})
	dic.update({"iword":iword})
	dic.update({"insight":insight})
	dic.update({"twothreepron":twothreepron})
	#dic.update({"wordcount":wordcount})
	dic.update({"punc":punc})
	#dic.update({"sentcount":sentcount})
	dic.update({"wordbysent":wordbysent})
	dic.update({"bigword":bigword})
	dic.update({"number":number})
	dic.update({"conjunc":conjunc})
	dic.update({"propnoun":propnoun})


	for key in dic.keys():
		for i in range(0,dic[key]):
			temp+=key+' '

	temp=temp.rstrip(' ')
	temp=line+' '+temp+'\n'
	outputtemp+=temp

outputfile.write(outputtemp)				


