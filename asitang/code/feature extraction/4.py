import nltk
import sys
inputfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
outputfile=open(sys.argv[1]+'.4','w')
outputtemp=''
for line in inputfile:
	line = nltk.word_tokenize(line)
	predict=line[0]
	line=nltk.pos_tag(line)
	temp=''
	for word in line:
		temp+=word[1]+' '
	temp=temp.rstrip(' ')+'\n'
	outputtemp+=predict+' '+temp

outputfile.write(outputtemp)