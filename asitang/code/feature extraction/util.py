import nltk
import sys
import string

def count(doc,wordlist):
	wordset=set()
	for line in wordlist:
		line=line.replace('\n','')
		wordset.add(line)	

	doc = nltk.word_tokenize(doc)
	count=0
	for word in doc:
		if word in wordset:
			count+=1
	return count

def countPOS(doc,postag):
	doc = nltk.word_tokenize(doc)
	doc=nltk.pos_tag(doc)
	count=0
	for word in doc:
		if word[1] == postag:
			count+=1
	return count

def word_count(doc):
    tokens=nltk.word_tokenize(doc)
    return len(tokens)

def sentence_count(doc):
    sentences=nltk.sent_tokenize(doc)
    return len(sentences)

def words_per_sentence(doc):
    token_length=len(nltk.word_tokenize(doc))
    sentences_length=len(nltk.sent_tokenize(doc))
    return int(token_length/sentences_length)

def big_word_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    for word in tokens:
        if len(word)>6:
            count+=1
    return count

def punc_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    punctuations=string.punctuation.replace('.','')
    for word in tokens:
        if word in punctuations:
            #print(word)
            count+=1
    return count

def number_count(doc):
    count=0
    tokens=nltk.word_tokenize(doc)
    for word in tokens:
        flag=0
        for ch in word:
            if ch in '0123456789':
                flag=1
                break
        if flag==1:
            count+=1
    return count




#inputfile=open(sys.argv[1],'r', encoding='cp1252', errors='ignore')
#doc='This hotel is ok but is not the best for the money . First of all the parking prices are exorbitant . I know parking is at a premium for this location but the price is much too high . Also , the service at this hotel was poor . I felt like room service took too long and considering the price the service was poor in general , and the staff at The Palmer House seemed more interested in helping the richer or more known customers more than us . In addition I feel that there were too many fees including a fee for internet . Also the rooms were very clean and beds very nice but there was some noise and you could hear the people walking above you . I would say it was an average experience at this hotel and considering the price maybe think twice about The Palmer House .'
#print(countPOS(doc,'NN'))
#print(countPOS(doc,inputfile))	

